package com.example.sphfluidsimulation;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.List;

public class Physics
{
    public static double Width = 0;
    public static double Height = 0;
    public static double Gravity = 0.98;
    public static double AirPressure = -1;
    public double density = 0;


    public void calculatePressure(){

        for (Particle particle : HelloController.particles){
            if(particle.density < density) particle.density = density;
            particle.pressure = particle.density - density;
        }
    }

    public static void calculateForce(){

    }





    public static void drawParticles(Pane pane, List<Particle> particles) {
        pane.getChildren().clear();

        for(Particle particle : particles)
        {
            pane.getChildren().add(particle);
        }
    }

    // Physics
    public static void moveParticles(List<Particle> particles) {
        for(Particle particle : particles)
        {
            particle.move();
        }
    }

}
