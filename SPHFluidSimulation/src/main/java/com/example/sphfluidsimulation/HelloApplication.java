package com.example.sphfluidsimulation;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class HelloApplication extends Application {

    public static Scene s = null;

    @Override
    public void start(Stage stage) throws IOException {
        HelloController controller = new HelloController();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        fxmlLoader.setController(controller);

        Scene scene = new Scene(fxmlLoader.load(), 500, 500);
        stage.setScene(scene);

        stage.show();
        controller.initialize();



    }

    public static void main(String[] args) {
        launch();
    }
}