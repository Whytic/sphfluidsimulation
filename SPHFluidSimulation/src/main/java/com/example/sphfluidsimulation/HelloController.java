package com.example.sphfluidsimulation;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class HelloController {

    @FXML
    Pane pane;

    public static List<Particle> particles = new ArrayList<>();

    public double randomDegrees() {
        return (int)(Math.random() * (360) + 1);
    }

    public double spawnCoordinate() {
        return (int)(Math.random() * (400 - 100)) + 100;
    }

    public List<Particle> spawnParticles(int n) {
        List<Particle> particles = new ArrayList<>();

        for(; n > 0; n--) {
            particles.add(new Particle(spawnCoordinate(), 450, 5, randomDegrees(), 0.005));
        }

        return particles;
    }

    public static int initialized = 0;

    public void initialize() {
        if(++initialized < 2)
            return;

        System.out.println("Started simulation");
        pane.setStyle("-fx-background-color: black;");

        Physics.Width = pane.getScene().getWidth();
        Physics.Height = pane.getHeight();

        particles = spawnParticles(3);

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                Physics.Width = pane.getScene().getWidth();
                Physics.Height = pane.getHeight();

                Physics.moveParticles(particles);
                Physics.drawParticles(pane, particles);
            }
        };
        timer.start();
    }
}