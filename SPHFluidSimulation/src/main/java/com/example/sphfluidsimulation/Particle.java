package com.example.sphfluidsimulation;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.List;

public class Particle extends Circle {

    public double[] velocity;
    public double[] velocity_preview;
    public double velocity_x;
    public double velocity_y;
    public double x;
    public double y;
    public double pressure;
    public double density;


    double direction;
    double speed;
    double momentum;
    double mass;

    public Particle(double x, double y, double radius, double direction_degrees, double mass) {
        this.setRadius(radius);
        this.setCenterX(x);
        this.setCenterY(Physics.Height - y);

        this.setFill(Color.CADETBLUE);
        this.mass = mass;
        this.direction = Math.toRadians(direction_degrees);
        this.momentum = 0;
    }

    public void X(double x) {
        this.setCenterX(X() + x);
    }

    public void Y(double y) {
        this.setCenterY(Physics.Height - Y() - y);
    }

    public double X() {
        return this.getCenterX();
    }

    public double Y() {
        return Physics.Height - this.getCenterY();
    }

    public double Radius() {
        return this.getRadius();
    }

    public void move() {

        edgeBounce();
        Y(this.momentum);
        this.momentum -= Physics.Gravity * 0.005;
        //System.out.println(this.momentum);
        //X(Math.cos(direction) * speed);
        //Y(Math.sin(direction) * speed);
        //Y(-0.98);




    }

    public void edgeBounce() {
        double new_X = X() + Math.cos(direction) * speed;
        double new_Y = Y() + Math.sin(direction) * speed;

        double new_Y_gravity = Y() - Physics.Gravity * 0.005;
        if (new_Y_gravity - Radius() < 0) {
            this.momentum = this.momentum * -1 * 0.7;
        }

        if (new_X + Radius() < 0 || new_X + Radius() > Physics.Width) {
            changeDirection(-1 * direction + Math.PI);
        }

        if (new_Y - Radius() < 0 || new_Y + Radius() > Physics.Height) {
            changeDirection(-1 * direction);
        }

    }

    public void changeDirection(double direction) {
        this.direction = direction;
    }
}
