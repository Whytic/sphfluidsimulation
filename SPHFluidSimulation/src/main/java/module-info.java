module com.example.sphfluidsimulation {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.sphfluidsimulation to javafx.fxml;
    exports com.example.sphfluidsimulation;
}